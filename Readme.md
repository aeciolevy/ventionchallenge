## Vention Code Challenge
## Aécio Levy - 19/11/2018

### Installation instrutions

Installation instructions
1. Unzip the file
2. On the root folder run the following command:
```
npm install
npm run installDependecies
```
3. To run (This command will start both client and server): 
```
npm run dev
```

## Environment Variables
A .env file is included with the database connection URL and the port to run the server. Also, one .env is included in the client folder with the specific port to run the client.

## Home Work Interview
Option choose B - A small application about a stapple network algorithm.

### Stacks

#### Client
* react
* react-d3-graph
* styled-components

#### Server
* express
* body-parser
* mongoose
* morgan

#### DBMS
* MongoDB

#### Solution

The minimum spanning tree is calculated with Kruskal's Algorithm.

The files with the Graph and Kruskal class are located at:
```
server
 |--- handlers
 |    |---graph.js
 |    |---MSTKruskal.js
```
## Application

### Client Side
- Single page application
- Fetch vertices and edge
- Render the graph
- Update edge distance (weight)
- Calculate Minimum Spanning Tree request

### Server Side
- Express server
- MongoDB (DBMS)
- End Points:
	- GET: /app/calculate-mst: Calculate Minimum Spanning tree based in the edge storage in the database.
	- POST: /edge: Insert a new edge
	- GET: /edge: Fetch all edges
    - PUT: /edge: Update one or many edges
	- POST: /vertice: Insert a new vertice
	- GET: /vertice: Fetch all vertices


## Demo App
![setup](./gifs/Demo.gif)
