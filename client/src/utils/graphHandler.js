import * as api from '../api';

class GraphHandler {

    static async loadData() {
        const [error, nodeData] = await api.getVertices();
        if (error) {
            console.log(error);
        }
        const nodes = this.parseNodes(nodeData.documents);
        const [errorEdge, edgeData] = await api.getEdges();
        if (errorEdge) {
            console.log(errorEdge);
        }
        const links = this.parseLinks(edgeData.documents);
        return { 
            nodes, 
            links,
        };
    }

    static parseNodes(nodes) {
        return nodes.map(el => ({ id: el.name }));
    }

    static parseLinks(links) {
        return links.map(el => ({ _id: el._id, source: el.verticeSource, target: el.verticeDestination, distance: el.distance}));
    }

    static async loadMST () {
        const [error, mst] = await  api.getMST();
        if (error) {
            console.log(error);
        }
        return mst;
    }

    static async submitUpdate(edgeToUpdate) {
        if (!edgeToUpdate) {
            throw new Error('Data to update is missed');
        }
        const arrayOfEdges = Object.values(edgeToUpdate);
        const objectToSend = { edges: arrayOfEdges };
        const [error, updated] = await api.updateEdges(objectToSend);
        if (error) {
            throw new Error(error);
        }
        console.log(error, updated)
        return updated;
    }
}

export default GraphHandler;
