import React from 'react';
import GraphHandler from '../utils/graphHandler'
import DivFlex from './DivFlex';
import MSTResult from './MSTResult';

class MSTContainer extends React.PureComponent {

    state = { MST: {}}

    handleClick = async () => {
        const calculatedMST = await GraphHandler.loadMST();
        this.setState({ MST: { ...calculatedMST } });
    }

    render() {
        const { MST } = this.state;
        const mstLength = Object.keys(MST).length;
        const { results } = MST;

        return(
            <DivFlex flow="column" alignItems="center" justify="center" width="100%">
                <button style={{ margin: '1rem', fontSize: '1.5rem', borderRadius: '0.3rem'}} onClick={this.handleClick}> 
                    Calculate MST 
                </button>
                { mstLength > 0 && 
                    <h2 style={{ color: 'white', marginTop:'0'}}> 
                    Minimum Spanning Tree Result
                    </h2>
                }
                { mstLength > 0 && <MSTResult results={results} />}
            </DivFlex>
        );
    }
}

export default MSTContainer;
