import React from 'react';
import { Graph } from 'react-d3-graph';

// the graph configuration, you only need to pass down properties
// that you want to override, otherwise default ones will be used
const myConfig = {
    nodeHighlightBehavior: true,
    node: {
        color: 'lightgreen',
        fontColor: 'white',
        size: 150,
        highlightStrokeColor: 'blue'
    },
    link: {
        highlightColor: 'lightblue'
    }
};

class GraphView extends React.PureComponent {    
    render() {
        const { data } = this.props;
        return(
            <>
                <Graph
                    id="graph-id" // id is mandatory, if no id is defined rd3g will throw an error
                    data={data}
                    config={myConfig}
                />
            </>
        );
    }
}

export default GraphView;
