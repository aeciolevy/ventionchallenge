import React from 'react';
import PropTypes from 'prop-types';
import DivFlex from './DivFlex';
import LinkItems from './LinksItems';
import MSTContainer from './MSTContainer';

class ShowEdgeWeight extends React.PureComponent {

    static propTypes = {
        links: PropTypes.array,
    }

    render() {
        const { links } = this.props;
        return(
            <DivFlex flow="column" alignItems="center" justify="center">
                <DivFlex flow="row" width="100%">
                    <LinkItems links={links}/>
                    <MSTContainer />
                </DivFlex>
            </DivFlex>
        );
    } 
}

export default ShowEdgeWeight;
