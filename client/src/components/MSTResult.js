import React from 'react';
import PropTypes from 'prop-types';
import DivFlex from './DivFlex';

class MSTResult extends React.PureComponent {

    static propTypes = {
        results: PropTypes.array.isRequired,
    }

    render() {
        const { results } = this.props;
        
        return(
            <React.Fragment>
                {results.map((el, index) => (
                    <DivFlex key={`${el.verticeDestination}${index}`}>
                        <h4 style={{ margin: '0.3rem', color: 'white' }}>
                            {`${el.verticeSource} - ${el.verticeDestination}: ${el.weight}`}
                        </h4>
                    </DivFlex>
                ))}
            </React.Fragment>
        );
    }
}

export default MSTResult;
