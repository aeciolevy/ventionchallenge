import styled from 'styled-components';
import PropTypes from 'prop-types';

const DivFlex = styled.div`
    align-self: ${({ alignSelf }) => alignSelf};
    background-color: ${({ background }) => background};
    display: flex;
    flex: ${({ flex }) => flex};
    color: ${ ({ color }) => color};
    font-family: ${ ({ font }) => font};
    flex-flow: ${({ flow }) => flow};
    justify-content: ${({ justify }) => justify};
    align-items: ${({ alignItems }) => alignItems};
    align-content: ${({ alignContent }) => alignContent};
    margin: ${({ margin }) => margin};
    height: ${({ height }) => height};
    width: ${({ width }) => width};
    padding: ${({ padding }) => padding};
`;

DivFlex.propTypes = {
    alignSelf: PropTypes.string,
    background: PropTypes.string,
    flex: PropTypes.string,
    color: PropTypes.string,
    font: PropTypes.string,
    flow: PropTypes.string,
    justify: PropTypes.string,
    alignItems: PropTypes.string,
    alignContent: PropTypes.string,
    margin: PropTypes.string,
    height: PropTypes.string,
    width: PropTypes.string,
    padding: PropTypes.string,
};

DivFlex.defaultProps = {
    alignSelf: 'unset',
    background: 'unset',
    flex: 'unset',
    color: 'initial',
    font: 'Arial, Helvetica, sans-serif',
    flow: 'unset',
    justify: 'unset',
    alignItems: 'unset',
    alignContent: 'unset',
    margin: '0',
    height: 'auto',
    width: 'auto',
    padding: 'auto',
};

export default DivFlex;
