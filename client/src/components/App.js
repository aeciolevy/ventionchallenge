import React, { Component } from 'react';
import '../App.css';
import Graph from './Graph';
import ShowEdgeWeight from './ShowEdgeWeight';
import GraphHandler from '../utils/graphHandler';

class App extends Component {
    
    state = { data: {}}

    async componentDidMount() {
        const dataToGraph = await GraphHandler.loadData();
        this.setState({ data: { ...dataToGraph } });
    }
    
    render() {
        const { data } = this.state;
        const dataLength = Object.keys(data).length;

        return (
            <div className="App">
                <header className="App-header">
                    Mininum Spanning Tree
                </header>
                <main className="App-main" style={{ padding: '2rem'}}>
                    { dataLength > 0 && <Graph data={data} /> }
                    {dataLength > 0 && <ShowEdgeWeight links={data.links} /> }
                </main>
            </div>
        );
    }
}

export default App;
