import React from 'react';
import PropTypes from 'prop-types';
import DivFlex from './DivFlex';

class Input extends React.PureComponent {

    static propTypes = {
        linkData: PropTypes.object,
        handleChange: PropTypes.func,
    }

    render() {
        const { linkData: { _id, source, target, distance }, handleChange } = this.props;

        return(
            <div>
                <DivFlex>
                    <label style={{ color: 'white', width: '14rem' }}> 
                        {`${source} - ${target}`} (km) 
                    </label>
                    <input type="number" name={_id} defaultValue={distance} 
                        onChange={handleChange}
                    />
                </DivFlex>
            </div>
        );
    }
}

export default Input;
