import React from 'react';
import PropTypes from 'prop-types';
import DivFlex from './DivFlex';
import Input from './Input';
import GraphHandler from '../utils/graphHandler';

class LinkItems extends React.PureComponent {

    static propTypes = {
        links: PropTypes.array,
    }

    state = { edges: {}, isFetching: false}

    handleChange = (event) => {
        const { value, name } = event.target;
        if (value && name) {
            const edges = {...this.state.edges, [name]:{ _id: name, distance: value }};
            this.setState({ edges });
        }
    }

    handleSubmit = async () => {
        this.setState({ isFetching: true }, async () => {
            await GraphHandler.submitUpdate(this.state.edges);
            this.setState({ isFetching: false })
        })
    }

    render() {
        const { links } = this.props;
        const { isFetching } = this.state;

        return(

            <DivFlex flow="column" alignItems="center" justify="space-evenly" width="100%">
                <DivFlex width="100%" justify="center">
                    <h2 style={{ color: 'white'}}> Links </h2>
                </DivFlex>
                {links.map(el => (
                    <Input key={el._id} linkData={el} handleChange={this.handleChange}/>
                ))}
                <DivFlex width="100%" justify="center" padding="2rem 0">
                    { isFetching && <div style={{ color: 'white' }}> Updating data... </div>}
                    <button onClick={this.handleSubmit} disabled={isFetching}> 
                        Update Distance 
                    </button>
                </DivFlex>
            </DivFlex>
        );
    }
}

export default LinkItems;
