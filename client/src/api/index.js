import { get, put } from '../utils/fetch';

export const getVertices = async () => get(`/vertice`);

export const getEdges = async () => get(`/edge`);

export const getMST = async () => get(`/app/calculate-mst`);

export const updateEdges = async (edges) => put('/edge', edges);
