/* eslint lines-between-class-members: 0 */
// Dependecies
const toAll = require('../utils/toAll');

// Kruskal agorithm based on 
// https://www.geeksforgeeks.org/kruskals-minimum-spanning-tree-algorithm-greedy-algo-2/
/**
 * Function to calculate MST using Kruskal Algorithm
 */
class MSTKruskal {
    /**
     * 
     * @param {object} graph 
     */
    constructor(graph, vertices, verticesCount) {
        this.graph = graph;
        this.vertices = vertices;
        this.verticesCount = verticesCount;
    }
    /**
     * find the parent of the vertice
     * @param {*} parent 
     * @param {*} i 
     */
    async find(parent, index, edge) {
        if (parent[index] === edge) {
            return edge;
        }
        let parentIndex = parent.indexOf(edge);
        return this.find(parent, parentIndex, edge);
    }
    /**
     * Union of two set os edgeSource and edgeDestination
     * @param {array} parent 
     * @param {array} rank 
     * @param {integer} edgeSource 
     * @param {integer} edgeDestination 
     */
    async union(parent, rank, edgeSource, edgeDestination) {
        const edgeSourceRoot = edgeSource;
        const edgeDestinationRoot = edgeDestination;

        if (rank[edgeSourceRoot] < rank[edgeDestinationRoot]) {
            parent[edgeSourceRoot] = edgeDestinationRoot;
        } else if (rank[edgeSourceRoot] > rank[edgeDestinationRoot]) {
            parent[edgeDestinationRoot] = edgeSourceRoot;
        } else {
            parent[edgeDestinationRoot] = edgeSourceRoot;
            rank[edgeSourceRoot] += 1;
        }
    }
    /**
     * Sorted a object based on the weight 
     * and return an Array
     * @param {object} element 
     */
    async sorted(element) {
        const elementArraySort = Object.values(element).sort((a, b) => a.weight - b.weight);
        return elementArraySort;
    }

    /**
     * Construct a Minimum Spanning Tree 
     * using Kruskal's algorithm
     */
    async calculateKruskalMST() {
        const results = [];
        let edgeIndex = 0;
        let resultIndex = 0;
        let parent = [];
        let rank = [];
        // sort graph by weight
        this.graph = await this.sorted(this.graph);
        for (let i = 0; i < this.verticesCount; ++i) {
            parent.push(this.vertices[i].name);
            rank.push(0);
        }

        while (resultIndex < (this.verticesCount - 1)) {
            const { verticeSource, verticeDestination, weight } = this.graph[edgeIndex];
            edgeIndex += 1;
            const sourceIndex = this.vertices.indexOf(verticeSource);
            let edgeSource = this.find(parent, sourceIndex, verticeSource);
            const destinationIndex = this.vertices.indexOf(verticeDestination);
            let edgeDestination = this.find(parent, destinationIndex, verticeDestination);

            const [, response] = await toAll([edgeSource, edgeDestination]);
            // If this edge does not causa a cycle 
            // include it to results
            [edgeSource, edgeDestination] = response;
            if (edgeSource !== edgeDestination) {
                resultIndex += 1;
                results.push({ verticeSource, verticeDestination, weight });
                await this.union(parent, rank, edgeSource, edgeDestination);
            }
        }
        const minimumWeight = results.reduce((acc, curr) => acc + curr.weight, 0);
        return { minimumWeight, results };
    }
}

module.exports = MSTKruskal;
