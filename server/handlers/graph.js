/* eslint lines-between-class-members: 0 */
/** 
 * Class represent a Graph
*/
class Graph {
    /**
     * 
     * @param {number} vertices 
     */
    constructor(vertices) {
        this.vertices = vertices;
        this.graph = {};
    }
    /**
     * 
     * @param {number} verticeSource
     * @param {number} verticeDestination
     * @param {*} weight 
     */
    addEdge(verticeSource, verticeDestination, weight) {
        this.graph[`edge${verticeSource}${verticeDestination}`] = {
            verticeSource,
            verticeDestination,
            weight,
        };
    }
    /**
     * return the graph
     */
    getGraph() {
        return this.graph;
    }
    /**
     * return the vertices of the graph
     */
    getVertices() {
        return this.vertices;
    }
}

module.exports = Graph;
