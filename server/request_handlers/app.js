// Dependecies
const Graph = require('../handlers/graph');
const MSTKruskal = require('../handlers/MSTKruskal');
const EdgeModel = require('../models/edges');
const VerticeModel = require('../models/vertices');
const to = require('../utils/to');
const { ErrorRequest, STATUS_CODE } = require('../utils/errorRequest');

// constant number of vertices
// save time to not get it from edges size
// TODO: n(n - 1)/2

exports.calculateMST = async (req, res, next) => {
    try {
        // query vertices
        const [errorVertices, vertices] = await to(VerticeModel.find({}, 'name'));
        if (errorVertices) {
            throw new ErrorRequest(STATUS_CODE.UNPROCESSABLE, errorVertices.message);
        }
        const VERTICES_COUNT = vertices.length;
        // query edges
        const [errorEdge, edges] = await to(EdgeModel.find());
        if (errorEdge) {
            throw new ErrorRequest(STATUS_CODE.UNPROCESSABLE, errorEdge.message);
        }
        const graph = new Graph(VERTICES_COUNT);
        edges.forEach(el => graph
            .addEdge(el.verticeSource, el.verticeDestination, el.distance));
        const mstKruskal = new MSTKruskal(graph.getGraph(), vertices, VERTICES_COUNT);
        const minMST = await mstKruskal.calculateKruskalMST();

        res.send(minMST);
    } catch (err) {
        next(err);
    }
};
