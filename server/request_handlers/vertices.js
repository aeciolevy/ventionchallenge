// Dependecies
const to = require('../utils/to');
const { ErrorRequest, STATUS_CODE } = require('../utils/errorRequest');
const RequestValidation = require('../utils/requestValidation');
const VerticeModel = require('../models/vertices');

exports.insertVertice = async (req, res, next) => {
    try {
        const [errorReq] = await to(RequestValidation
            .validateRequired(req.body, ['name', 'location']));
        if (errorReq) {
            throw new ErrorRequest(STATUS_CODE.BAD_REQUEST, errorReq.message);
        }

        const [errorCreate, document] = await to(VerticeModel.insertVertice(req.body));
        if (errorCreate) {
            throw new ErrorRequest(STATUS_CODE.UNPROCESSABLE, errorCreate.message);
        }

        res.send({ ...document._doc }); /* eslint no-underscore-dangle: 0 */
    } catch (err) {
        next(err);
    }
};

exports.getAllVertices = async (req, res, next) => {
    try {
        const [error, documents] = await to(VerticeModel.find());
        if (error) {
            throw new ErrorRequest(STATUS_CODE.UNPROCESSABLE, error.message);
        }

        res.send({ documents });
    } catch (err) {
        next(err);
    }
};
