// Dependecies
const to = require('../utils/to');
const toAll = require('../utils/toAll');
const { ErrorRequest, STATUS_CODE } = require('../utils/errorRequest');
const RequestValidation = require('../utils/requestValidation');
const EdgeModel = require('../models/edges');

exports.insertEdge = async (req, res, next) => {
    try {
        const [errorReq] = await to(RequestValidation
            .validateRequired(req.body, ['distance', 'verticeSource', 'verticeDestination']));
        if (errorReq) {
            throw new ErrorRequest(STATUS_CODE.BAD_REQUEST, errorReq.message);
        }

        const [errorCreate, document] = await to(EdgeModel.insertEdge(req.body));
        if (errorCreate) {
            throw new ErrorRequest(STATUS_CODE.UNPROCESSABLE, errorCreate.message);
        }

        res.send({ ...document._doc }); /* eslint no-underscore-dangle: 0 */
    } catch (err) {
        next(err);
    }
};

exports.getAllEdges = async (req, res, next) => {
    try {
        const [error, documents] = await to(EdgeModel.find());
        if (error) {
            throw new ErrorRequest(STATUS_CODE.UNPROCESSABLE, error.message);
        }

        res.send({ documents });
    } catch (err) {
        next(err);
    }
};

exports.updateEdges = async (req, res, next) => {
    try {
        const { edges } = req.body;
        const arrayAsync = edges.map(async el => EdgeModel.findOneAndUpdate(
            { _id: el._id },
            { distance: el.distance },
            { new: true },
        ));
        const [error, documents] = await toAll(arrayAsync);
        if (error) {
            throw new ErrorRequest(STATUS_CODE.UNPROCESSABLE, error.message);
        }
        res.send({ documents });
    } catch (err) {
        next(err);
    }
}
