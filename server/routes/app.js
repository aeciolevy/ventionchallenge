const express = require('express');
const AppReqHandler = require('../request_handlers/app');

const router = express.Router();

router.get('/calculate-mst', AppReqHandler.calculateMST);

module.exports = router;
