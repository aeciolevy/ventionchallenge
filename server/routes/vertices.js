const express = require('express');
const VerticeReqHandler = require('../request_handlers/vertices');

const router = express.Router();

router.post('/', VerticeReqHandler.insertVertice);
router.get('/', VerticeReqHandler.getAllVertices);

module.exports = router;
