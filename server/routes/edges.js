const express = require('express');
const EdgeReqHandler = require('../request_handlers/edges');

const router = express.Router();

router.post('/', EdgeReqHandler.insertEdge);
router.get('/', EdgeReqHandler.getAllEdges);
router.put('/', EdgeReqHandler.updateEdges);

module.exports = router;
