const path = require('path');
require('dotenv').config({ path: path.join(__dirname, '.env') });

const PORT = process.env.PORT || 4000;
const mongoose = require('mongoose');
const Server = require('./server');
// DB Setup
if (process.env.NODE_ENV === 'development') {
    const db = process.env.MONGODB;
    mongoose.connect(db, { useNewUrlParser: true })
        .then(() => {
            console.log('DB Connected.');
            const server = new Server();
            server.init(PORT);
        })
        .catch(err => console.log(err))
}
