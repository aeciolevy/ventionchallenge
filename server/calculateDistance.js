const path = require('path');
require('dotenv').config({ path: path.join(__dirname, '.env') });
const mongoose = require('mongoose');

const VerticesModel = require('./models/vertices');

const db = process.env.MONGODB;
mongoose.connect(db, { useNewUrlParser: true })
    .then(() => {
        console.log('DB Connected.');
    });

console.log('start');
VerticesModel.geoSearch({type: 'Point'}, {
    near: [40.724, -73.997],
    distanceField: "dist.calculated", // required
    maxDistance: 0.008,
    query: { type: "public" },
    includeLocs: "dist.location",
    uniqueDocs: true,
})
.then(result => console.log(result))
.catch(err => console.log(err));
