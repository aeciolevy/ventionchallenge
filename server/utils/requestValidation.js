
/**
 * validate keys that are required
 * in the request body
 * @param {object} data
 * @param {array} required
 */
exports.validateRequired = async (data, required) => {
    let keys = Object.keys(data);
    let keyMissed = required.reduce((acc, curr, i, arr) => {
        if (!keys.includes(curr)) {
            acc = curr;
            arr.splice(i);
            return acc;
        }
        return acc;
    }, '');
    if (keyMissed) {
        throw new Error(`${keyMissed} is missed`);
    }
    return true;
};
