// Dependencies
const mongoose = require('mongoose');
const to = require('../utils/to');

const edgeSchema = new mongoose.Schema({
    verticeSource: {
        type: String,
        trim: true,
        required: true,
    },
    verticeDestination: {
        type: String,
        trim: true,
        required: true,
    },
    distance: {
        type: Number,
        min: 0,
        required: true,
    },
});

edgeSchema.statics.insertEdge = async function insertEdge(data) {
    const [error, document] = await to(this.create({ ...data }));
    if (error) {
        throw new Error(error);
    }
    return document;
};

const EdgeModel = mongoose.model('edge', edgeSchema);

module.exports = EdgeModel;
