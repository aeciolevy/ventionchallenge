// Dependecies
const mongoose = require('mongoose');
const to = require('../utils/to');

const pointSchema = new mongoose.Schema({
    type: {
        type: String,
        enum: ['Point'],
        required: true,
    },
    coordinates: {
        type: [Number],
        required: true,
    },
});

const verticeSchema = new mongoose.Schema({
    name: {
        type: String,
        trim: true,
        unique: true,
    },
    location: {
        type: pointSchema,
        required: true,
    },
});

verticeSchema.statics.insertVertice = async function insertVertice(data) {
    const [error, document] = await to(this.create({ ...data }));
    if (error) {
        throw new Error(error);
    }
    return document;
}

const VerticeModel = mongoose.model('vertice', verticeSchema);

module.exports = VerticeModel;
