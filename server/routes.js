// Files and Controllers
const VerticesRoutes = require('./routes/vertices');
const EdgeRoutes = require('./routes/edges');
const AppRoutes = require('./routes/app');

module.exports = (app) => {
    // API entry points
    app.use('/vertice', VerticesRoutes);
    app.use('/edge', EdgeRoutes);
    app.use('/app', AppRoutes);
};
