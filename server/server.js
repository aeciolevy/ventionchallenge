// Modules
const express = require('express');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const route = require('./routes');

class Server {
    init(port) {
        const app = express();
        // Setup Server
        app.use(bodyParser.json());
        app.use(bodyParser.urlencoded({ extended: true }));
        app.use(morgan('dev'));
        // Serve static files
        app.use(express.static('build'));
        route(app);
        // Error Handling middleware
        app.use((err, req, res, next) => {
            if (err instanceof Error) {
                return res.status(err.status || 500)
                    .send({ status: err.status, message: err.message });
            }
            next(err);
        });
        app.listen(port, () => console.log(`server running on port ${port}`));
    }
}

module.exports = Server;
